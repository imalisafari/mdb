//
//  DetailView.swift
//  Movies DB
//
//  Created by Ali Safari on 2/19/21.
//  Copyright © 2021 Ali Safari. All rights reserved.
//

import UIKit

class DetailView: UIView {

    @IBOutlet public weak var imageView: UIImageView!
    @IBOutlet public weak var titleLabel: UILabel!
    @IBOutlet public weak var plotLabel: UILabel!
    @IBOutlet public weak var activityIndicator: UIActivityIndicatorView!

}
