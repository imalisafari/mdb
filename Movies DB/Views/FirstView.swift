//
//  FirstView.swift
//  Movies DB
//
//  Created by Ali Safari on 2/19/21.
//  Copyright © 2021 Ali Safari. All rights reserved.
//

import UIKit

class FirstView: UIView {

    @IBOutlet public weak var textField: UITextField!
    @IBOutlet public weak var searchBtn: UIButton!
    
}
