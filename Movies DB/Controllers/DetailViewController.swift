//
//  DetailViewController.swift
//  Movies DB
//
//  Created by Ali Safari on 2/19/21.
//  Copyright © 2021 Ali Safari. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    public var detailView: DetailView! {
        guard isViewLoaded else { return nil }
        return (view as! DetailView)
    }
    
    public var movieInfo = MovieInfo()
    public var searchQuery = ""
    public var firstViewController: FirstViewController!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        fetchReq(for: searchQuery)

    }

    private func fetchReq(for query: String) {
        let urlString = "https://www.omdbapi.com/?t=\(query)&apikey=77fab3b"
        let url = URL(string: urlString)!
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            

            guard let data = data, error == nil else {
                print("error**************")
                let message = "Connection failed."
                DispatchQueue.main.async {
                    self.alertsetUp(with: message)
                }
                
                return
            }
                DispatchQueue.main.async {
                    do {

                        self.movieInfo = try JSONDecoder().decode(MovieInfo.self, from: data)

                        self.detailView.titleLabel.text = self.movieInfo.Title
                        self.detailView.plotLabel.text = self.movieInfo.Plot
                        self.imageFetcher()
                    } catch {
                        print("Error Decoding json__________________")
                    }
                }
        }.resume()
    }
    
    private func imageFetcher() {
        if let urlString = movieInfo.Poster {
            print(urlString)
            let url = URL(string: urlString)!
            
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                guard let data = data, error == nil else {
                    print("error**************")
                    let message = "The image isn't avalibe."
                    DispatchQueue.main.async {
                        self.alertsetUp(with: message)
                    }
                    return
                }
                DispatchQueue.main.async {
                    self.detailView.imageView.image = UIImage(data: data)
                    
                    self.detailView.activityIndicator.stopAnimating()
                }
            }.resume()
        } else {
            let message = "The image isn't avalibe."
            alertsetUp(with: message)
        }
    }
    
    public func alertsetUp(with message: String) {
        let alert = UIAlertController(title: "Oops :(", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Okay", style: .cancel, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
}

extension DetailViewController: UIAdaptivePresentationControllerDelegate {
    
    func presentationControllerWillDismiss(_ presentationController: UIPresentationController) {
        print("im done will dis")
        firstViewController.returnSequence()
    }
    func presentationControllerDidDismiss(_ presentationController: UIPresentationController) {
        print("im done")
//        firstViewController.returnSequence()
    }
}
