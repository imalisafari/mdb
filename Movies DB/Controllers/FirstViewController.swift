//
//  ViewController.swift
//  Movies DB
//
//  Created by Ali Safari on 2/19/21.
//  Copyright © 2021 Ali Safari. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {
    
    public var firstView: FirstView! {
        guard isViewLoaded else { return nil }
        return (view as! FirstView)
    }

    private var searchQuery = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        navigationController?.presentationController?.delegate = DetailViewController()
        
        
            
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let detailVC = segue.destination as? DetailViewController {

            detailVC.firstViewController = self
            detailVC.presentationController?.delegate = detailVC
            detailVC.searchQuery = self.searchQuery
        }
    }
    
    
    @IBAction public func search(_ sender: Any) {
        if let txt = firstView.textField.text {
            if firstView.textField.text != "" {
                self.searchQuery = titleCorrection(for: txt)
                performSegue(withIdentifier: "firstToDetail", sender: (Any).self)
            } else {
                //Some Animation for textField
            }
        }
    }
    
    private func titleCorrection(for title: String) -> String {
        var theText = ""
        for i in title {
            if i == " " {
                theText.append("-")
            } else {
                theText.append(i)
            }
        }
        return theText
    }
    
    public func returnSequence() {
        print("delegation worked finally 656565656656566565v665")

        firstView.textField.text = ""
    }
    
}

