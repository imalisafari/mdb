//
//  MovieInfoModel.swift
//  Movies DB
//
//  Created by Ali Safari on 2/19/21.
//  Copyright © 2021 Ali Safari. All rights reserved.
//

import Foundation

struct MovieInfo: Codable {
    var Title: String?
    var Year: String?
    var BoxOffice: String?
    var Director: String?
    var Poster: String?
    var Plot: String?
}
